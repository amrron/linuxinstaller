#!/bin/bash
################################################################################
################################################################################
########################  AmRRON Setup Script v0.5.3.3  ########################
########################        Created by TB-14        ########################
########################          10 Feb 2019           ########################
################################################################################
################################################################################
#
# Chainsaw's Conky Test version

#### Simplfied get country info for IP. curl ipinfo.io/country simply returns the country, no awk'ng
### Fixed eth info in standard to not have WiFi info
### Used VPN check from my conky that finds tun in routes but also shows VPN Down
### Added if_empty for cpus incase thay don't have 4. (cpu0 = average of all)
### Conky start up should be 'conky -p5 -d' to deamonize it after pausing a few seconds to allow the system to come up
# Install qTox on Ubuntu 16 or greater. Should work on Ubuntu 16 or 18 based
# systems including Mint
# Tested on Ubuntu 18.10 and Mint 19.1
################################################################################
#########  Change log  #########################################################
################################################################################
VERSION="0.1"
# Version 0.1 - First Draft
# Updated to use AmRRON Install Security folder

# Setting variables that might be used if put in master Script
#Defaults
CONKYSEL='STANDARD'
ERRLOG="/tmp/conky-failures.txt"
STARTUP="y"

# Master install shoud pass SecDir and Error log, if desired.
for i in "$@"
do
case $i in
    -s=*|--size=*)
    CONKYSEL="${i#*=}"
    CONKYSEL="$(echo $CONKYSEL | tr '[a-z]' '[A-Z]')"
    ;;
      -l=*|--log=*)
    ERRLOG="${i#*=}"
    ;;
    -s=*|--startup=*)
    STARTUP="${i#*=}"
    STARTUP="$(echo $STARTUP | tr '[A-Z]' '[a-z]')"
    ;;
    *)
    # unknown option
    echo "Unknown option passed to script." >> $ERRLOG
    ;;
esac
done
# Debug info to ERRLOG
echo "Installing Conky application. Installer version $VERSION " >> $ERRLOG
echo "CONKYSEL = ${CONKYSEL}  ERRLOG = ${ERRLOG} STARTUP = $STARTUP" >> $ERRLOG



function conky_install() {

if ! hash conky; then
sudo apt-get -y install conky-all ||
        { echo "Failed to install Conky" >> $ERRLOG; return; }
fi

if [[ -f $HOME/.conkyrc ]]; then
mv $HOME/.conkyrc $HOME/.conkyrc-$(date +%h%d).bak ||
        { echo "Failed to backup old .conkyrc file" >> $ERRLOG; return; }
fi
######## Move Conky.Config before size case statement since so much is shared --CS

	cat > $HOME/.conkyrc <<\EOF
conky.config = {
    alignment = 'top_right',
    background = false,
    border_width = 0,
	border_inner_margin = 15,
    color1 = '19A094',
    color2 = 'FF5252',
    cpu_avg_samples = 2,
	default_color = 'grey',
    default_outline_color = 'grey',
    default_shade_color = 'grey',
	double_buffer = true,
	draw_borders = true,
    draw_graph_borders = false,
    draw_outline = false,
    draw_shades = false,
    use_xft = true,
    font = 'Monaco:size=12',
    gap_x = 20,
    gap_y = 50,
    minimum_height = 5,
    maximum_width = 400,
    net_avg_samples = 2,
    no_buffers = true,
    out_to_console = false,
    out_to_stderr = false,
    extra_newline = false,
    own_window = true,
    own_window_hints = 'undecorated,below,sticky,skip_taskbar,skip_pager',
    own_window_class = 'Conky',
    own_window_type = 'desktop',
    own_window_transparent = true,
    own_window_argb_visual = true,
    own_window_argb_value = 0,
    stippled_borders = 0,
    update_interval = 1.2,
    uppercase = none,
    use_spacer = 'left',
    show_graph_scale = false,
    show_graph_range = false
}
EOF

case $CONKYSEL in
	LARGE)
    # Use Conky.Config as is, append conky.text
	cat >> $HOME/.conkyrc <<\EOF
conky.text = [[
${color1}${font Roboto Mono:style=Bold:pixelsize=22}\
Pacific:${goto 175}Eastern:${alignr}Zulu:
$color $hr
${color grey}${font Roboto:pixelsize=22}${tztime America/Los_Angeles %H:%M:%S}\
${goto 175}${tztime America/New_York %H:%M:%S}\
${alignr}${tztime Zulu %H:%M:%S}
${font Roboto:pixelsize=12}${tztime America/Los_Angeles %A %m-%d-%y}\
${goto 175}${tztime America/New_York %A %m-%d-%y}\
${alignr}${tztime Zulu %A %m-%d-%y}

$alignc ${font Roboto:style=Medium:pixelsize=35} ${color grey}\
${color D4AF37} BTC: ${execi 60 echo -n "$" ; curl -s https://api.coinbase.com/v2/prices/spot?currency=USD | grep -o [0-9][0-9][0-9][0-9].[0-9][0-9]}
#Uptime:$color $uptime
${color1}${font Roboto Mono:style=Bold}\
SYSTEM$color $hr\
${font}
${color2}CPU : ${color1}${goto 240}\
$cpu% ${goto 280}${cpubar cpu0 10,140}
${color2}RAM :${color grey} $mem/$memmax\
 ${color1}${goto 240}$memperc% ${goto 280}${membar 10,140}
${color2}Swap:${color grey} $swap/$swapmax\
 ${color1}${goto 240}$swapperc% ${goto 280}${swapbar 10,140}
${color2}Temp: ${color grey}${acpitemp}°C
${color white}CPU1: ${color1}${cpu cpu1}% ${cpubar cpu1 10,100}\
  ${color white}${goto 220}CPU2: ${color1}${cpu cpu2}% ${cpubar cpu2 10,100}
${color white}CPU3: ${color1}${cpu cpu3}% ${cpubar cpu3 10,100}\
  ${color white}${goto 220}CPU4: ${color1}${cpu cpu4}% ${cpubar cpu4 10,100}
${color2}/ ${color grey}\
 ${goto 90}${fs_used /}/${fs_size /}${color}\
 ${goto 250}${color grey}${color1}${fs_used_perc /}% ${fs_bar 10,120 /}
${color2}/home ${color grey}\
 ${goto 90}${fs_used /home}/${fs_size /home}${color}\
 ${goto 250}${color grey}${color1}${fs_used_perc /home}% ${fs_bar 10,120 /home}

${color1}${font Roboto Regular:style=Bold}\
NETWORKING$color${font} $hr
${color2}External IP:$color ${exec curl -s www.icanhazip.com}${alignr}${color2}Country: $color${execi 60 curl ipinfo.io/country}
${if_up eth0}\
${color2}eth0\
 ${color}${font}${goto 270}${addrs eth0}
 ${color}Total:${totaldown eth0} \
 ${goto 210}${color}Total:${totalup eth0}
${downspeedgraph eth0 30,195 00ffff 19a094} \
${upspeedgraph eth0 30,195 00ffff 19A094}
${endif}\
${if_up wlp1s0}\
${color}${font}\
${color2}wlp1s0: ${color}${wireless_essid wlp1s0}\
 ${color}${font}${goto 270}${addr wlp1s0}
 ${color white}${font}Strength:$color${wireless_link_qual_perc wlp1s0}%\
 ${goto 200}${color white}MAC: ${color}${wireless_ap wlp1s0}
 ${color}Total:${totaldown wlp1s0} \
 ${goto 210}${color}Total:${totalup wlp1s0}
${downspeedgraph wlp1s0 30,195 00ffff 19A094} \
${upspeedgraph wlp1s0 30,195 00ffff 19A094}
${endif}\
${if_up tun0}\
${color}${font}\
${color2}tun0\
 ${color}${font}${goto 270}${addr tun0}
 ${color}Total:${totaldown tun0} \
 ${goto 210}${color}Total:${totalup tun0}
${downspeedgraph tun0 30,195 00ffff 19A094} \
${upspeedgraph tun0 30,195 00ffff 19A094}${endif}
${if_empty ${exec cat /proc/net/route | grep tun}}${color red}VPN DOWN${color}${else}${color green}VPN UP${color}${endif}
${alignr}${if_running tor}${color green}TOR ACTIVE${else}${color red}TOR OFF$endif

${color1}${font Roboto Mono:style=Bold}\
PROCESSES$color$font $hr
Total:$processes  Running:$running_processes
${color2} Name             ${goto 205}PID         ${goto 275}CPU%     ${goto 350}MEM%
${color} ${top name 1} ${goto 200}${top pid 1} ${goto 275}${top cpu 1} ${goto 350}${top mem 1}
${color} ${top name 2} ${goto 200}${top pid 2} ${goto 275}${top cpu 2} ${goto 350}${top mem 2}
${color} ${top name 3} ${goto 200}${top pid 3} ${goto 275}${top cpu 3} ${goto 350}${top mem 3}
${color} ${top name 4} ${goto 200}${top pid 4} ${goto 275}${top cpu 4} ${goto 350}${top mem 4}
${color} ${top name 5} ${goto 200}${top pid 5} ${goto 275}${top cpu 5} ${goto 350}${top mem 5}

${color1}${font Roboto Mono:style=Bold}\
AmCON Status $color$font $hr

${color grey}${font Roboto Mono:style=Bold}${alignc}Currently at ${execi 300 curl -s "https://amrron.com/net-resourcestools/amcon-amrron-communications-condition-level/" | grep -m1 -o "AmCON-1\|AmCON-2\|AmCON-3\|AmCON-4\|AmCON-5"}

${color1}${font Roboto Mono:style=Bold}\
Radio Tools $color$font $hr

${if_running ardopc}${color green}ARDOP ACTIVE${else}${color red}ARDOP OFF$endif \
${goto 200}${if_running pat}${color green}PAT ACTIVE${else}${color red}PAT OFF$endif
]]
EOF
	;;

	STANDARD)
    ### Edit ConkyConfig Large
    sed -i 's/Monaco:size=12/Monaco:size=10/' $HOME/.conkyrc
    sed -i 's/maximum_width = 400/maximum_width = 300/'  $HOME/.conkyrc

###  append conky.text --CS
	cat >> $HOME/.conkyrc <<\EOF
conky.text = [[
${color1}${font Roboto Mono:style=Bold:pixelsize=14}\
Pacific:${goto 135}Eastern:${alignr}Zulu:
$color $hr
${color grey}${font Roboto:pixelsize=12}${tztime America/Los_Angeles %H:%M:%S}\
${goto 135}${tztime America/New_York %H:%M:%S}\
${alignr}${tztime Zulu %H:%M:%S}
${font Roboto:pixelsize=12}${tztime America/Los_Angeles %m-%d-%y}\
${goto 135}${tztime America/New_York %m-%d-%y}\
${alignr}${tztime Zulu %m-%d-%y}

#Uptime:$color $uptime
${color1}${font Roboto Mono:style=Bold}\
SYSTEM$color $hr\
${font}
${color2}CPU : ${color1}${goto 180}\
$cpu% ${goto 210}${cpubar cpu0 10,105}
${color2}RAM :${color grey} $mem/$memmax\
 ${color1}${goto 180}$memperc% ${goto 210}${membar 10,105}
${color2}Temp: ${color grey}${acpitemp}°C


${color2}/ ${color grey}\
 ${goto 65}${fs_used /}/${fs_size /}${color}\
 ${goto 180}${color grey}${color1}${fs_used_perc /}% ${goto 210}${fs_bar 10,105 /}
${color2}/home ${color grey}\
 ${goto 65}${fs_used /home}/${fs_size /home}${color}\
 ${goto 180}${color grey}${color1}${fs_used_perc /home}% ${goto 210}${fs_bar 10,105 /home}

${color1}${font Roboto Regular:style=Bold}\
NETWORKING$color${font} $hr
${color2}External IP:$color ${exec curl -s www.icanhazip.com}${alignr}${color2}Country: $color${execi 60 curl ipinfo.io/country}
${if_up eth0}\
${color2}eth0\
 ${color}${font}${goto 270}${addrs eth0}
 ${color}Total:${totaldown eth0} \
 ${goto 210}${color}Total:${totalup eth0}
${downspeedgraph eth0 30,195 00ffff 19a094} \
${upspeedgraph eth0 30,195 00ffff 19A094}
${endif}\
${if_up wlp1s0}\
${color}${font}\
${color2}wlp1s0: ${color}${wireless_essid wlp1s0}\
 ${color}${font}${goto 230}${addr wlp1s0}
 ${color white}${font}Strength:$color${wireless_link_qual_perc wlp1s0}%\
 ${goto 155}${color white}MAC: ${color}${wireless_ap wlp1s0}
 ${color}Total Down:${totaldown wlp1s0} \
 ${goto 200}${color}Total Up:${totalup wlp1s0}
${endif}
${if_up tun0}\
${color}${font}\
${color2}VPN tun0\
 ${color}${font}${goto 230}${addr tun0}
 ${color}Total Down:${totaldown tun0} \
 ${goto 200}${color}Total Up:${totalup tun0}${endif}

${if_empty ${exec cat /proc/net/route | grep tun}}${color red}VPN DOWN${color}${else}${color green}VPN UP${color}${endif}
${if_running tor}${color green}TOR ACTIVE${else}${color red}TOR OFF$endif

${color1}${font Roboto Mono:style=Bold}\
AmCON Status $color$font $hr

${color grey}${font Roboto Mono:style=Bold}${alignc}Currently at ${execi 300 curl -s "https://amrron.com/net-resourcestools/amcon-amrron-communications-condition-level/" | grep -m1 -o "AmCON-1\|AmCON-2\|AmCON-3\|AmCON-4\|AmCON-5"}

${color1}${font Roboto Mono:style=Bold}\
Radio Tools $color$font $hr

${if_running ardopc}${color green}ARDOP ACTIVE${else}${color red}ARDOP OFF$endif \
${goto 200}${if_running pat}${color green}PAT ACTIVE${else}${color red}PAT OFF$endif
]]
EOF
	;;

	MINIMAL)
	### Edit ConkyConfig Large
    sed -i 's/own_window_argb_visual = true/own_window_argb_visual = false/'  $HOME/.conkyrc

    ###  append conky.text --CS
	cat >> $HOME/.conkyrc <<\EOF

conky.text = [[

${color1}${font Roboto Mono:style=Bold}\
AmCON Status $color$font $hr

${color grey}${font Roboto Mono:style=Bold}${alignc}Currently at ${execi 300 wget -q -O - "https://amrron.com/net-resourcestools/amcon-amrron-communications-condition-level/ "|grep -m1 "AmCON-1\|AmCON-2\|AmCON-3\|AmCON-4\|AmCON-5" -o | head -n1}

${if_running piardopc}${color green}ARDOP ACTIVE${else}${color red}ARDOP INACTIVE${endif}
${if_running pat http}${color green}PAT ACTIVE${else}${color red}PAT INACTIVE${endif}
]]
EOF
	;;
esac ||
        { ERRCODE="Failed to create .conkyrc configuration file."; errors; return; }

if [[ ${PLAT} = "Raspbian" ]]; then
sed -i '/own_window_argb_value/d' $HOME/.conkyrc ||
        { ERRCODE="Failed to remove own_window_argb_value from .conkyrc for raspbian stability"; errors; return; }

sed -i '/own_window_argb_visual/d' $HOME/.conkyrc ||
        { ERRCODE="Failed to remove own_window_argb_visual from .conkyrc for raspbian stability"; errors; return; }

sed -i s/'desktop'/'normal'/ $HOME/.conkyrc ||
        { ERRCODE="Failed to change own_window_type to normal for raspbian stability."; errors; return; }
fi

#### Find ethernet and wifi interfaces and update conkyrc --CS
## default ethernet = eth0  default wifi = wlp1s0
##  use 2nd field from ip link and word starts with e for all ethernet, w for all wireless
ethernet_interface=$(ip link | cut -d: -f2 | grep  '\be')
wifi_interface=$(ip link | cut -d: -f2 | grep  '\bw')

### Test to make sure the string isnt blank (doesnt have that type of interface
[ $ethernet_interface ] && ( sed -i "s/eth0/$ethernet_interface/g" $HOME/.conkyrc )
[ $wifi_interface ]     && ( sed -i "s/wlp1s0/$wifi_interface/g" $HOME/.conkyrc  )


echo conkysetup >> /tmp/cleanup

}



#############
# Run the test function

conky_install

## Add conky to the start up after login
if [[ $STARTUP = "y" ]] || [[ $STARTUP = "yes" ]]  ; then
   mkdir -p  ~/.config/autostart
   echo "[Desktop Entry]
Encoding=UTF-8
Version=0.9.4
Type=Application
Name=Conky
Comment=System Monitor
Exec=/usr/bin/conky -p 8 -d
StartupNotify=false
Terminal=false
Hidden=false" >  ~/.config/autostart/conky.desktop
fi
