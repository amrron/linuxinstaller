#!/bin/bash
################################################################################
################################################################################
########################  qTox Install Script v0.1      ########################
########################        Created by Chainsaw     ########################
########################          21 Feb 2019           ########################
################################################################################
################################################################################

# Install qTox on Ubuntu 16 or greater. Should work on Ubuntu 16 or 18 based
# systems including Mint
# Tested on Ubuntu 18.10 and Mint 19.1
################################################################################
#########  Change log  #########################################################
################################################################################
VERSION="0.1"
# Version 0.1 - First Draft
# Updated to use AmRRON Install Security folder

# Setting variables that might be used if put in master Script
#Defaults
SECDIR="$HOME/Security/"
ERRLOG="/tmp/qtox-failures.txt"
DESKICON="y"

# Master install shoud pass SecDir and Error log, if desired.
for i in "$@"
do
case $i in
    -d=*|--directory=*)
    SECDIR="${i#*=}"
    ;;
      -l=*|--log=*)
    ERRLOG="${i#*=}"
    ;;
    -i=*|--icon=*)
    DESKICON="${i#*=}"
    DESKICON="$(echo $DESKICON | tr '[A-Z]' '[a-z]')"
    ;;
    *)
    # unknown option
    echo "Unknown option passed to script." >> $ERRLOG
    ;;
esac
done
# Debug info to ERRLOG
echo "Installing qTox application. Installer version $VERSION " >> $ERRLOG
echo "SECDIR = ${SECDIR}  ERRLOG = ${ERRLOG} DESKICON = $DESKICON" >> $ERRLOG



echo "Checking and installing prerequsits ....."
sudo apt-get -y -qq install  build-essential  cmake  libavcodec-dev \
    libavdevice-dev  libavfilter-dev  libavutil-dev  libexif-dev \
    libgdk-pixbuf2.0-dev  libglib2.0-dev  libgtk2.0-dev  libkdeui5 \
    libopenal-dev  libopus-dev  libqrencode-dev  libqt5opengl5-dev \
    libqt5svg5-dev  libsodium-dev  libsqlcipher-dev  libswresample-dev \
    libswscale-dev  libvpx-dev  libxss-dev  qrencode  qt5-default \
    qttools5-dev-tools  qttools5-dev  libconfig-dev  git


#create a dir to hold all the Tox tools in one dir.
#Use the AmRRON script Security folder
mkdir -p $SECDIR/tox

# Install toxcore
cd $SECDIR/tox
git clone https://github.com/toktok/c-toxcore.git toxcore
cd toxcore
git checkout v0.2.9
cmake .
make -j$(nproc)
sudo make install

# we don't know what whether user runs 64 or 32 bits, and on some distros
# (Fedora, openSUSE) lib/ doesn't link to lib64/, so add both
echo '/usr/local/lib64/' | sudo tee -a /etc/ld.so.conf.d/locallib.conf
echo '/usr/local/lib/' | sudo tee -a /etc/ld.so.conf.d/locallib.conf
sudo ldconfig

# Install qTox
cd $SECDIR/tox
git clone https://github.com/qTox/qTox.git qTox
cd qTox
cmake .
make -j$(nproc)
sudo make install

#  Add Icon to  Desktop
if [[ $DESKICON = "y" ]] || [[ $DESKICON = "yes" ]]  ; then
   echo "Creating qTox desktop icon."
    cp /usr/local/share/applications/io.github.qtox.qTox.desktop $HOME/Desktop/qtox.desktop
    chmod +x $HOME/Desktop/qtox.desktop
fi
# done
