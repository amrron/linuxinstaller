# LinuxInstaller

Scripts to automate the setup of Linux Mint for use by AmRRON radio operators

Run this script after completing chapters 1 & 2 of the Linux Workstation Guide


# Install These Scripts
Install `git` then clone this project to your computer
```shell
    sudo apt install git
    git clone https://gitlab.com/amrron/linuxinstaller.git LinuxInstaller
```
Getting updates
```shell
    cd ~/LinuxInstaller
    git pull
```


# Notes to developers
If you want to provide updates:

Add the files to the project locally. Use dot `.` for all files or specify the
individual file you're updating.
```shell
    git add .
or
    git add README.md
```

Then commit the change with an optional comment. All files that were just added
will have the same commit comment.
```shell
    git commit -m "Updated Readme file."
```
Push the project up to the server
```shell
    git push origin master
```
