#!/bin/bash
################################################################################
################################################################################
########################    Linux Mint 19 Setup Tool    ########################
########################        Created by Chainsaw     ########################
########################          21 Feb 2019           ########################
################################################################################
################################################################################

# Complete the install of a new Linux MInt 19 system
# systems including Mint

################################################################################
#########  Change log  #########################################################
################################################################################
VERSION="0.1"
# Version 0.1 - First Draft
# Updated to use AmRRON Install Security folder

######################## Check os for Mint 19 or better
PRETTY_NAME=$(cat /etc/*-release |grep PRETTY_NAME |gawk -F= '{ print $2}')
echo "Running $PRETTY_NAME"
if [[ PRETTY_NAME = "Linux Mint 19.1" ]]
then
    echo "OS is Tested, proceding....."
esle
  echo "Untested OS. Abort"
  return
fi

########################  flash & restrited esentails
sudo apt-get -y install flashplugin-installer ubuntu-restricted-extras curl git
sudo apt-get -y install imagemagick
sudo apt-get update
sudo apt-get -y upgrade
sudo apt-get -y dist-upgrade
sudo apt-get autoclean
sudo apt-get autoremove

######################## add user to serial/USB groups
sudo  usermod -a -G dialout $USER
sudo  usermod -a -G tty $USER
sudo  usermod -a -G audio $USER
sudo  usermod -a -G vbox $USER

######################### Install AmRRON LinuxInstaller Scripts
cd ~/
git clone https://gitlab.com/amrron/linuxinstaller.git LinuxInstaller

########################  Email – ThunderBird & Emigmail
sudo apt-get install gnupg2 thunderbird enigmail gpa

######################## Firefox
# No programs to add

########################  TOR #############################################
sudo apt install torbrowser-launcher

######################## Steg
# http://www.fabionet.org
cd ~/Security/
wget -qO - http://www.fabionet.org/download/steg-v1.0.0.2-linux64.tgz | tar xvz -C ~/Security/
mv ~/Security/steg-v1.0.0.2-linux64 ~/Security/Steg
echo "[Desktop Entry]
Type=Application
Name=Steg
Comment=Easy cross platform steganography
Exec=$HOME/Security/Steg/steg
Icon=steg.png
Terminal=false
Type=Application
Categories=Graphics; " > $HOME/Desktop/steg.desktop
chmod +x $HOME/Desktop/steg.desktop
sudo cp  $HOME/Desktop/steg.desktop /usr/local/share/applications/steg.desktop

wget -qO  /tmp/steg88.png \
   http://www.fabionet.org/sites/default/themes/mythemes/acq_broadway/images/88x88x32-stegIcon.png
sudo convert /tmp/steg88.png --resize 24x24 /usr/local/share/icons/hicolor/24x24/apps/steg.png

######################## Retroshare
# Use retroshare/xUbuntu_18.04 as Mint 19 is based on it
wget -qO - https://download.opensuse.org/repositories/network:/retroshare/xUbuntu_18.04/Release.key | sudo apt-key add -
sudo sh -c "echo 'deb https://download.opensuse.org/repositories/network:/retroshare/xUbuntu_18.04/ /' >> /etc/apt/sources.list.d/retroshare_OBS.list"
sudo apt-get -qq update
sudo apt-get -y install retroshare

######################## qTox
~/LinuxInstaller/qTox-install.sh

#cleanup
sudo apt-get autoclean
sudo apt-get autoremove
